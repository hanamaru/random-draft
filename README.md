'Teban54's Pokémon GO analysis toolkit', with the code to extract data from Pokebattler, as well as spreadsheets and images for all past articles (in ``data/counters/``).

The main program is ``test_main.py``.

More documentation to follow...