o
    2Sd�e  �                   @   s<   d Z ddlT ddlT ddlT G dd� d�ZG dd� d�ZdS )aV  
This module includes two main types of utilities:
- A class for a list of criteria that attackers have to satisfy.
- Utilities for dealing with attacker ensembles.

An AttackerCriteria object contains a collection of criteria that attackers need to satisfy.
Its main goal is to choose which attackers to analyze from the rankings lists.

The following filters can be added to a single AttackerCriteria object.
For an attacker to "meet" the AttackerCriteria, it has to pass ALL specified filters.
- Charged move type(s). This is an approximation for "attacker type", and should be used
  primarily for type-based filtering, instead of Pokemon types.
  Rationale:
    - Some attackers function without STAB: Shadow Ball Mewtwo, Shadow Ball Darkrai, etc.
    - Some attackers use fast and charged moves of different types in some situations:
      Swampert (MS/HC), Blaziken (Counter/BB), Mewtwo (PC/SB), Terrakion (SD/SS), etc.
    - Some attackers with type A/B prefer to use type A moves against a certain boss,
      which would skew the results if compared with attackers of type B.
      Garchomp against Reshiram and Zekrom is perhaps the most well-known example.
    - In both cases, charged move typing usually does the best in describing what "type"
      of attacker the Pokemon is functionally acting as.
        - Counter/Blast Burn Blaziken is a perfectly fine option, and everyone considers it as
          a fire attacker. No need to force Fire Spin on it in situations where Counter is better.
        - This does have the issue of using Dragon Tail/Earth Power Garchomp at times,
          or doing weird things to Terrakion (and Smack Down/Crunch Tyranitar against Lugia),
          but overall it's worth the tradeoff.
  If multiple types are included, all Pokemon with charged moves of these types will be considered.
- Minimum level, maximum level, and level step size.
- Whether the attacker is or is not a legendary, shadow or mega.
- Pokebattler Trainer ID. If specified, only attackers from that user's Pokebox will be used.
  Still subject to other filters, e.g. only consider grass attackers from the Pokebox.
- Specific Pokemon names (code names). If specified, only these Pokemon will be considered.
  Still subject to other filters.
- Pokemon type(s). Only use this if you know what you're doing.
  If multiple types are included, all Pokemon with these types will be considered.
- Fast move type(s). Only use this if you know what you're doing.

AttackerCriteriaMulti class is used as an "OR" of multiple AttackerCriteria objects.
I.e. An attacker that satisfies any AttackerCriteria object is eligible for AttackerCriteriaMulti.
Examples: Grass attackers in my Pokebox OR grass attackers by level, L40 shadows OR L50 non-shadows.
Since AttackerCriteriaMulti includes more APIs, it's recommended to build an AttackerCriteriaMulti
even if only one set of criteria is used.
�    )�*c                   @   sd   e Zd ZdZddddeeeddddddddddddddfdd�Z			ddd�Zdd	� Z	d
d� Z
dS )�AttackerCriteriaz;
    Class for a single set of criteria for attackers.
    NFc                 C   s�   dd� }||�| _ ||�| _||�| _|| _|| _|| _|| _|	| _|
| _|| _	|| _
|| _|| _|| _|| _|| _|| _|| _|| _|| _|| _| jrXt| j�tu rX| jg| _| jrgt| j�tu rg| jg| _| jrxt| j�tu rz| jg| _dS dS dS )a]
  
        Initialize all filters.

        :param metadata: Current Metadata object
        :param pokemon_types: Types of Pokemon to be considered, as list of natural names ("Dragon")
            or a single string
        :param fast_types: Types of fast moves to be considered, as list of natural names ("Dragon")
            or a single string
        :param charged_types: Types of fast moves to be considered, as list of natural names ("Dragon")
            or a single string.
            This should be used as the primary way to filter attackers by type.
        :param min_level: Minimum attacker level, inclusive
        :param max_level: Maximum attacker level, inclusive
        :param level_step: Step size for level, either 0.5 or integer
        :param pokemon_codenames: List of codenames of all Pokemon so that attackers are restricted to
            these options, if necessary. Still subject to other filters.
            If None, all attackers will be considered.
        :param pokemon_codenames_and_moves: List of tuples of Pokemon codenames and movesets
            so that attackers are restricted to these options, if necessary. Still subject to other filters.
            Possibly including IVs.
            Format:
            [
                ("URSALUNA", "MUD_SHOT_FAST", "HIGH_HORSEPOWER"),
                ("URSALUNA", "TACKLE_FAST", "HIGH_HORSEPOWER"),
                ("GOLURK_SHADOW_FORM", "MUD_SLAP_FAST", "EARTH_POWER"),
                ("GARCHOMP_MEGA", "MUD_SHOT_FAST", "EARTH_POWER", "10/10/10"),
                ...
            ]
            If None, all attackers will be considered.
        :param trainer_id: Pokebattler Trainer ID if a trainer's own Pokebox is used.
            If None, use all attackers by level.
        :param is_legendary: If True, only consider legendary attackers
        :param is_not_legendary: If True, only consider non-legendary attackers
        :param is_mythical: If True, only consider mythical attackers
        :param is_not_mythical: If True, only consider non-mythical attackers
        :param is_legendary_or_mythical: If True, only consider legendary or mythical attackers
        :param is_not_legendary_or_mythical: If True, only consider non-legendary or mythical attackers
        :param is_shadow: If True, only consider shadow attackers
        :param is_not_shadow: If True, only consider non-shadow attackers
        :param is_mega: If True, only consider mega attackers
        :param is_not_mega: If True, only consider non-mega attackers
        :param exclude_codenames: List of codenames of all Pokemon to be excluded, regardless of other filters.
        c                 S   s6   | du s| sdS t t| �tu r| n| g�}|sdS |S )z�
            Parse the input types.
            :param types: None, a string or a list mixed with code and natural names.
            :return: Well-formatted list, or None if the filter shouldn't be active
            N)�parse_type_codes2strs�type�list)�types�parsed� r	   �5D:\Games\Pokemon GO\random-draft\attacker_criteria.py�parse_typesj   s   z.AttackerCriteria.__init__.<locals>.parse_typesN)�pokemon_types�
fast_types�charged_types�	min_level�	max_level�
level_step�pokemon_codenames�pokemon_codenames_and_moves�
trainer_id�is_legendary�is_not_legendary�is_mythical�is_not_mythical�is_legendary_or_mythical�is_not_legendary_or_mythical�	is_shadow�is_not_shadow�is_mega�is_not_mega�metadata�exclude_codenamesr   �str�tuple)�selfr   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r    r   r	   r	   r
   �__init__6   s:   4




�zAttackerCriteria.__init__c
                 C   s�  |s|st dtjd� dS | j�|�}d}
| js| jr.|s.|s(t dtjd� d}
| j�|�}d}| js6| jrI|sI|sCt dtjd� d}| j�|�}d}| jdur�|	rTdS d}|r�|r�| jD ]'}|d |j	kr�|d	 |j	kr�|d
 |j	kr�t
|�dks�|d |kr�d} q�q]t| j p�t|| j�| j p�|
p�|j| jv | j p�|p�|j| jv | p�t|| j| j�| j p�|j	| jv |t|| j| j�t|| j| j�t|| j| j�t|| j| j�t|| j | j!�| j" p�|j	| j"vg�S )a�  
        Check if a particular attacker with fast and charged moves meets ALL criteria
        in this AttackerCriteria object.
        Note:
        - Trainer ID is not checked. Instead, it's applied to Pokebattler simulation settings.
        - If fast or charged moves are not given, those comparisons are IGNORED.

        :param pokemon_codename: Pokemon codename
        :param pokemon: Pokemon object, if codename is not given
        :param level: Level of attacker, as string or numerical value
        :param iv: IV of attacker, as string "15\15\15"
        :param fast_codename: Fast move codename
        :param fast: Fast Move object, if codename is not given
        :param charged_codename: Charged move codename
        :param charged: Charged Move object, if codename is not given
        :param ignore_specific_codenames_and_moves: If True, always returns False if this AttackerCriteria includes
            a check for Pokemon codenames and moves. The effect of this is that an attacker can only pass
            AttackerCriteriaMulti via single AttackerCriteria objects that do not focus on a whitelist, but
            on generic criteria (e.g. typing).
            This is used to determine eligibility for estimator scaling (IN_SCALING): For example, Black Kyurem
            against itself will not pass the generic AttackerCriteria, but it passes the whitelist AttackerCriteria.
            We don't want Black Kyurem to participate in estimator scaling, so we need a way to prevent it from
            passing the whitelist for these purposes.
            Default to False.
        :return: Whether the attacker meets all criteria
        zVError (AttackerCriteria.check_attacker): Neither Pokemon object nor codename provided.��fileFz�Warning (AttackerCriteria.check_attacker): Neither fast move object nor codename provided. IGNORING the fast move typing and/or specific Pokemon check.Tz�Warning (AttackerCriteria.check_attacker): Neither charged move object nor codename provided. IGNORING the charged move typing and/or specific Pokemon check.Nr   �   �   �   )#�print�sys�stderrr   �find_pokemonr   r   �	find_mover   �name�len�allr   �criterion_is_typesr   �is_level_in_ranger   r   r   �criterion_legendaryr   r   �criterion_mythicalr   r   �criterion_legendary_or_mythicalr   r   �criterion_shadowr   r   �criterion_megar   r   r    )r#   �pokemon�pokemon_codename�levelZiv�fastZfast_codenameZchargedZcharged_codenameZ#ignore_specific_codenames_and_movesZfast_not_givenZcharged_not_givenZpass_pokemon_moves_check�pkmr	   r	   r
   �check_attacker�   sh   ���

*
�
���zAttackerCriteria.check_attackerc                 C   s$  t � }| jdur| j�� nd|_| jdur| j�� nd|_| jdur'| j�� nd|_| j|_| j|_| j|_| jdur@| j�� nd|_| j	durM| j	�� nd|_	| j
|_
| j|_| j|_| j|_| j|_| j|_| j|_| j|_| j|_| j|_| j|_| j|_| jdur�| j�� |_|S d|_|S )z|
        Create a deep copy of this AttackerCriteria object.
        :return: Copy of this AttackerCriteria object.
        N)r   r   �copyr   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r   r    )r#   �cpr	   r	   r
   r?   �   s2   �zAttackerCriteria.copyc                 C   s�   | j sg S g }dg}| jrtdtjd� n	t| j| j| j�}| j D ],}|D ]'}|�	|d |t
|�dkr7|d nd|d |d	 t
|�d
krH|d ndf� q%q!|S )a�  
        Get the list of required attacker codenames, their levels, IVs and movesets specified in this object,
        specifically the pokemon_codenames_and_moves attribute.
        These attackers are forced to be included in the output CSV. If they're not in the counters lists, they
        should be filled during fill_blanks.
        IV is taken as 15\15\15 by default, unless specified in self.pokemon_codenames_and_moves.
        This should only be used if this object is by level, not trainer ID. Thus, levels are generated based on
        the configurations here.

        :return: List of required attacker codenames, their levels, IVs, movesets and whether they participate in
            estimator scaling (None if it's left to the default settings). Format:
            [
                ("URSALUNA", 30, "15\15\15", "MUD_SHOT_FAST", "HIGH_HORSEPOWER", None),
                ("URSALUNA", 40, "15\15\15", "MUD_SHOT_FAST", "HIGH_HORSEPOWER", None),
                ("URSALUNA", 30, "15\15\15", "TACKLE_FAST", "HIGH_HORSEPOWER", None),
                ("URSALUNA", 40, "15\15\15", "TACKLE_FAST", "HIGH_HORSEPOWER", None),
                ("GOLURK_SHADOW_FORM", 30, "15\15\15", "MUD_SLAP_FAST", "EARTH_POWER", None),
                ("GOLURK_SHADOW_FORM", 40, "15\15\15", "MUD_SLAP_FAST", "EARTH_POWER", None),
                ("GARCHOMP_MEGA", 30, "10\10\10", "MUD_SHOT_FAST", "EARTH_POWER", None),
                ("GARCHOMP_MEGA", 40, "10\10\10", "MUD_SHOT_FAST", "EARTH_POWER", None),
                ("SCIZOR_MEGA", 30, "15\15\15", "FURY_CUTTER_FAST", "X_SCISSOR", True),  # Force to participate in scaling
                ("SCIZOR_MEGA", 40, "15\15\15", "FURY_CUTTER_FAST", "X_SCISSOR", True),  # Force to participate in scaling
                ...
            ]
        �(   z�Warning (AttackerCriteria.get_required_attackers): Method called on AttackerCriteria based on Trainer IDs. 
Please only use it on criteria by level.
Using Level 40 as default.r%   r   �   r)   z15\15\15r'   r(   �   N)r   r   r*   r+   r,   �get_levels_in_ranger   r   r   �appendr0   )r#   �retZ	lvl_ranger=   �lvlr	   r	   r
   �get_required_attackers  s(   �
��z'AttackerCriteria.get_required_attackers)	NNNNNNNNF)�__name__�
__module__�__qualname__�__doc__�MIN_LEVEL_DEFAULT�MAX_LEVEL_DEFAULT�LEVEL_STEP_DEFAULTr$   r>   r?   rH   r	   r	   r	   r
   r   2   s$    
�^
�Pr   c                   @   s�   e Zd ZdZddd�Zdd� Zdd� Zd	d
� Zdd� Zdd� Z	dd� Z
dd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd� ZdS ) �AttackerCriteriaMultiz�
    Class for multiple sets of criteria for attackers, such that an attacker only
    needs to satisfy any one set of criteria.

    Many functions in this class are to get values for Pokabattler API.
    Nc                 C   s   || _ || _dS )zG
        Initialize with all sets as AttackerCriteria objects.
        N)�setsr   )r#   rQ   r   r	   r	   r
   r$   ;  s   
zAttackerCriteriaMulti.__init__c                    s   t � fdd�| jD ��S )a�  
        Check if a particular attacker with fast and charged moves satisfies
        ANY of the individual AttackerCriteria objects.

        :param pokemon_codename: Pokemon codename
        :param pokemon: Pokemon object, if codename is not given
        :param level: Level of attacker, as string or numerical value
        :param iv: IV of attacker, as string "15\15\15"
        :param fast_codename: Fast move codename
        :param fast: Fast Move object, if codename is not given
        :param charged_codename: Charged move codename
        :param charged: Charged Move object, if codename is not given
        :param ignore_specific_codenames_and_moves: If True, always returns False if this AttackerCriteria includes
            a check for Pokemon codenames and moves. The effect of this is that an attacker can only pass
            AttackerCriteriaMulti via single AttackerCriteria objects that do not focus on a whitelist, but
            on generic criteria (e.g. typing).
            This is used to determine eligibility for estimator scaling (IN_SCALING): For example, Black Kyurem
            against itself will not pass the generic AttackerCriteria, but it passes the whitelist AttackerCriteria.
            We don't want Black Kyurem to participate in estimator scaling, so we need a way to prevent it from
            passing the whitelist for these purposes.
            Default to False.
        :return: Whether the attacker meets any individual AttackerCriteria
        c                 3   s    � | ]}|j di � ��V  qd S )Nr	   )r>   ��.0�criteria��kwargsr	   r
   �	<genexpr>Z  s   � z7AttackerCriteriaMulti.check_attacker.<locals>.<genexpr>��anyrQ   )r#   rV   r	   rU   r
   r>   B  s   z$AttackerCriteriaMulti.check_attackerc                 C   s    t tdd� | jD ���}t|�S )z�
        Return all individual attacker levels that needs to be considered across
        all AttackerCriteria sets.
        :return: List of all levels to be considered
        c                 S   s.   g | ]}t |j|j|j�D ]}|js|�qqS r	   )rD   r   r   r   r   )rS   rT   rG   r	   r	   r
   �
<listcomp>b  s    ���z4AttackerCriteriaMulti.all_levels.<locals>.<listcomp>)r   �setrQ   �sorted)r#   �
all_levelsr	   r	   r
   r]   \  s   

�z AttackerCriteriaMulti.all_levelsc                 C   �   t dd� | jD ��S )z�
        Return the minimum level that needs to be considered across all AttackerCriteria sets.
        :return: Minimum level to be considered
        c                 s   �   � | ]}|j V  qd S �N)r   rR   r	   r	   r
   rW   o  �   � z2AttackerCriteriaMulti.min_level.<locals>.<genexpr>)�minrQ   �r#   r	   r	   r
   r   j  �   zAttackerCriteriaMulti.min_levelc                 C   r^   )z�
        Return the maximum level that needs to be considered across all AttackerCriteria sets.
        :return: Maximum level to be considered
        c                 s   r_   r`   )r   rR   r	   r	   r
   rW   v  ra   z2AttackerCriteriaMulti.max_level.<locals>.<genexpr>)�maxrQ   rc   r	   r	   r
   r   q  rd   zAttackerCriteriaMulti.max_levelc                 C   r^   )z�
        Checks whether the "show legendaries" option in Pokebatter needs to be turned on.
        :return: Value of the "show legendaries" option to be used for Pokebattler API
        c                 s   s$   � | ]}|j p|jo|j V  qd S r`   )r   r   r   rR   r	   r	   r
   rW   }  s   � �
�z>AttackerCriteriaMulti.pokebattler_legendary.<locals>.<genexpr>rX   rc   r	   r	   r
   �pokebattler_legendaryx  s   �z+AttackerCriteriaMulti.pokebattler_legendaryc                 C   r^   )z�
        Checks whether the "show shadows" option in Pokebatter needs to be turned on.
        :return: Value of the "show shadows" option to be used for Pokebattler API
        c                 s   �   � | ]}|j  V  qd S r`   )r   rR   r	   r	   r
   rW   �  �   � z;AttackerCriteriaMulti.pokebattler_shadow.<locals>.<genexpr>rX   rc   r	   r	   r
   �pokebattler_shadow�  rd   z(AttackerCriteriaMulti.pokebattler_shadowc                 C   r^   )z�
        Checks whether the "show megas" option in Pokebatter needs to be turned on.
        :return: Value of the "show megas" option to be used for Pokebattler API
        c                 s   rg   r`   )r   rR   r	   r	   r
   rW   �  rh   z9AttackerCriteriaMulti.pokebattler_mega.<locals>.<genexpr>rX   rc   r	   r	   r
   �pokebattler_mega�  rd   z&AttackerCriteriaMulti.pokebattler_megac                 C   s0   t dd� | jD ��rdS ttdd� | jD ���S )a�  
        If all individual sets of AttackerCriteria has a limit on Pokemon types,
        return the union of all these sets. This can be used in Pokebattler API
        to limit the types of attackers included.
        If any AttackerCriteria does not enforce a limit on Pokemon types, return None.
        :return: List of attacker Pokemon types in code name, or None if should not be enforced
        c                 s   s"   � | ]}|j  o|jd u V  qd S r`   )r   r   rR   r	   r	   r
   rW   �  s   �  zBAttackerCriteriaMulti.pokebattler_pokemon_types.<locals>.<genexpr>Nc                 S   s&   g | ]}|j r|j D ]}t|��q
qS r	   )r   �parse_type_str2code)rS   rT   �tpr	   r	   r
   rZ   �  s    ���zCAttackerCriteriaMulti.pokebattler_pokemon_types.<locals>.<listcomp>)rY   rQ   r   r[   rc   r	   r	   r
   �pokebattler_pokemon_types�  s
   

�z/AttackerCriteriaMulti.pokebattler_pokemon_typesc                 C   s   t tdd� | jD ���S )z�
        Return all Pokebattler Trainer IDs that will possibly be needed for simulations.
        Excludes all None values.
        :return: List of Pokebattler trainer IDs in all sets of criteria.
        c                 S   s   g | ]}|j r|j �qS r	   �r   rR   r	   r	   r
   rZ   �  s    �zAAttackerCriteriaMulti.pokebattler_trainer_ids.<locals>.<listcomp>)r   r[   rQ   rc   r	   r	   r
   �pokebattler_trainer_ids�  s   

�z-AttackerCriteriaMulti.pokebattler_trainer_idsc                 C   �   t dd� | jD �| jd�S )a  
        Get a new AttackerCriteriaMulti object that contains all AttackerCriteria without
        trainer IDs, i.e. all criteria with attacker levels.
        :return: AttackerCriteriaMulti object that contains all AttackerCriteria without
        trainer IDs
        c                 S   s   g | ]}|j s|�qS r	   rn   rR   r	   r	   r
   rZ   �  �    zCAttackerCriteriaMulti.get_subset_no_trainer_ids.<locals>.<listcomp>�r   �rP   rQ   r   rc   r	   r	   r
   �get_subset_no_trainer_ids�  �   �z/AttackerCriteriaMulti.get_subset_no_trainer_idsc                 C   rp   )z�
        Get a new AttackerCriteriaMulti object that contains all AttackerCriteria with
        trainer IDs.
        :return: AttackerCriteriaMulti object that contains all AttackerCriteria with
        trainer IDs
        c                 S   s   g | ]}|j r|�qS r	   rn   rR   r	   r	   r
   rZ   �  rq   z@AttackerCriteriaMulti.get_subset_trainer_ids.<locals>.<listcomp>rr   rs   rc   r	   r	   r
   �get_subset_trainer_ids�  ru   z,AttackerCriteriaMulti.get_subset_trainer_idsc                 C   rp   )z�
        Create a deep copy of this AttackerCriteriaMulti object.
        Each individual AttackerCriteria will also be copied.
        :return: Copy of this AttackerCriteriaMulti object.
        c                 S   s   g | ]}|� � �qS r	   )r?   rR   r	   r	   r
   rZ   �  s    z.AttackerCriteriaMulti.copy.<locals>.<listcomp>rr   rs   rc   r	   r	   r
   r?   �  s   �zAttackerCriteriaMulti.copyc                 C   s,   t � }| jD ]}|�t |�� �� qt|�S )a�  
        Get the list of required attacker codenames, their levels, IVs and movesets specified in this object,
        specifically the pokemon_codenames_and_moves attributes of individual AttackerCriteria objects.
        These attackers are forced to be included in the output CSV. If they're not in the counters lists, they
        should be filled during fill_blanks.
        IV is taken as 15/15/15 by default, unless specified in self.pokemon_codenames_and_moves.
        This should only be used if this object is by level, not trainer ID. Thus, levels are generated based on
        the configurations here.

        :return: List of required attacker codenames, their levels, IVs and movesets, Format:
            [
                ("URSALUNA", 30, "15\15\15", "MUD_SHOT_FAST", "HIGH_HORSEPOWER"),
                ("URSALUNA", 40, "15\15\15", "MUD_SHOT_FAST", "HIGH_HORSEPOWER"),
                ("URSALUNA", 30, "15\15\15", "TACKLE_FAST", "HIGH_HORSEPOWER"),
                ("URSALUNA", 40, "15\15\15", "TACKLE_FAST", "HIGH_HORSEPOWER"),
                ("GOLURK_SHADOW_FORM", 30, "15\15\15", "MUD_SLAP_FAST", "EARTH_POWER"),
                ("GOLURK_SHADOW_FORM", 40, "15\15\15", "MUD_SLAP_FAST", "EARTH_POWER"),
                ("GARCHOMP_MEGA", 30, "10\10\10", "MUD_SHOT_FAST", "EARTH_POWER"),
                ("GARCHOMP_MEGA", 40, "10\10\10", "MUD_SHOT_FAST", "EARTH_POWER"),
                ...
            ]
        )r[   rQ   �updaterH   r   )r#   rF   rT   r	   r	   r
   rH   �  s   
z,AttackerCriteriaMulti.get_required_attackersr`   )rI   rJ   rK   rL   r$   r>   r]   r   r   rf   ri   rj   rm   ro   rt   rv   r?   rH   r	   r	   r	   r
   rP   4  s     
	


	rP   N)rL   �utils�paramsr9   r   rP   r	   r	   r	   r
   �<module>   s    ,  