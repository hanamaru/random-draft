[Ice]
Now: Kyurem (Glaciate), Hisuian Avalugg

Future Pokemon: Black Kyurem, White Kyurem, Galarian Darmanitan Zen, Frosmoth, Glastrier (no ice fast move), Calyrex Ice Rider (Confusion), Iron Bundle (PS/IB,Bz,Av,IP,IW), Baxcalibur (IS,IF/IW,IB,Av,Bz,IS), Chien-Pao (PS,IS,IF/IW,Av,Bz)

Future shadows: Shadow Glaceon, Shadow Darmanitan (Galarian Standard)

Future megas: Mega Mewtwo Y

Better regular moves: Kyurem (Ice fast move), Black Kyurem (Ice Beam), Mega Glalie (IF), Mega Abomasnow (Av)

Signature moves: Black Kyurem (Freeze Shock), White Kyurem (Ice Burn), Hisuian Avalugg (Mountain Gale), Crabominable (Frost Breath Gen7/Ice Hammer), Calyrex Ice Rider (Glacial Lance)
* Use Avalanche and Glaciate

GamePress only: Iron Bundle (PS/Av,PS/Bz), Baxcalibur (IF/Av,IS/IB), Chien-Pao (IF/Av, IS/Bz), Shadow Darmanitan (Galarian Standard)


†
