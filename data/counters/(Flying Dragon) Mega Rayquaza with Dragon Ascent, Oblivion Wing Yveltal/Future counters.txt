[Flying]
Now: Rayquaza (Dragon Ascent), Mega Rayquaza (Dragon Ascent)

Future Pokemon: Enamorus-I (Fairy Wind/Fly), Iron Jugulis (Air Slash, Air Cutter/Acrobatics/Fly/Hurricane), Flamigo (Peck/Wing Attack/Air Slash, Brave Bird/Acrobatics/Aerial Ace/Air Cutter/Fly/Hurricane/Sky Attack), Kilowattrel (Peck/Air Slash, Hurricane/Acrobatics/Aerial Ace/Air Cutter/Fly/Brave Bird/Feather Dance)

Future shadows: Shadow Rayquaza, Shadow Archeops, Shadow Braviary, Shadow Yanmega, Shadow Togekiss

Future megas: 

Better regular moves: 
Salamence (Air Slash, Fly/Hurricane/Aerial Ace) (Shadow/Mega), 
//Thundurus-T (Fly/Acrobatics), 
Aerodactyl (Wing Attack, Fly/Sky Attack) (Shadow/Mega), 
Charizard (Fly/Hurricane/Acrobatics/Aerial Ace) (Shadow/Mega Y), 
Archeops (Acrobatics/Sky Attack), 
//Landorus-T (Fly), 
Mega Pidgeot (Fly), 
Tornadus-I (Gust, Acrobatics/Fly), 
Dragonite (Wing Attack/Air Slash, Fly) (Shadow), 
Shaymin-Sky (Air Slash/Aerial Ace), 
Zapdos (Peck, Sky Attack/Fly) (Shadow), 
Galarian Zapdos (Peck, Drill Peck/Fly), 
Moltres (Gust, Fly) (Shadow), 
Galarian Articuno (Gust/Air Slash, Fly), 
Yveltal (Sky Attack/Acrobatics), 
Honchkrow (Wing Attack/Gust, Fly) (Shadow), 
Ho-Oh (Gust/Air Slash, Sky Attack/Fly) (Shadow), 
Staraptor (Fly/Sky Attack) (Shadow), 
Braviary (Wing Attack, Sky Attack/Fly) (Shadow), 
Unfezant (Gust, Fly) (Shadow), 
Togekiss (Sky Attack/Fly) (Shadow), 
Toucannon (Fly/Brave Bird/Sky Attack), 
Lugia (Gust) (Shadow/Apex)

(Gust > Wing Attack > Air Slash > Peck, Fly > Sky Attack > Brave Bird > Drill Peck > Acrobatics > Hurricane > Aerial Ace > Air Cutter)
(Ignored most fast move improvements to prevent the list from getting too long, unless the original fast move was Peck or nothing)

Signature moves: Tornadus-I, Yveltal
* Use Brave Bird, Sky Attack, Fly, Aeroblast and Dragon Ascent to approximate

GamePress only: 

