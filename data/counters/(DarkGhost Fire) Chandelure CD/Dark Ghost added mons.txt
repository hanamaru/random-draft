Future Pokemon: Aegislash (can't), Zoroark, Hisuian Zoroark, Hisuian Typhlosion, Hisuian Samurott, Ash Greninja (can't), Dhelmise, Lunala, Dawn Wings Necrozma, Marshadow, Blacephalon, Polteageist, Grimmsnarl, Cursola, Dragapult, Urshifu Single Strike, Spectrier, Calyrex Shadow Rider

Future shadows and megas: Mega Banette, Mega Mewtwo Y, Mega Tyranitar, Shadow Gengar (Lick and Shadow Claw), Shadow Chandelure, Shadow Hydreigon, Shadow Giratina-O, Shadow Darkrai

Other Poltergeist users: Gengar, Giratina, Decidueye, Dhelmise, Lunala, Marshadow, Polteageist, Cursola

Other Brutal Swing users: Gyarados (Mega), Tyranitar (& Shadow, Mega), Absol (& Mega), Krookodile, Greninja (& Ash), Incineroar, Zarude

Signature moves: Giratina-O, Darkrai, Decidueye, Incineroar, Lunala, Marsahdow, Zoroark, Hisuian Zoroark, Pangoro, Hoopa (Unbound), Grimmsnarl, Urshifu Single Strike, Calyrex Shadow Rider

Better regular moves: Tyranitar (Snarl/Foul Play or Brutal Swing; & Shadow, Mega), Darkrai (Foul Play), Aegislash Blade (Shadow Claw), Lunala (Shadow Claw or Hex), Spectrier (Hex), Calyrex Shadow Rider (Hex), Urshifu Single Strike (Snarl/Foul Play)



†


Dark and Ghost attackers ranked by their average in-raid performance using Pokebattler Estimator, aka Average Scaled Estimator (ASE). Without and with dodging respectively.

Dark and Ghost attackers ranked by their average in-raid performance using Pokebattler TTW, aka Average Scaled Time to Win (ASTTW). Without and with dodging respectively.

Dark and Ghost attackers ranked by DPS^3*TDO and DPS respectively.


Fire attackers ranked by their average in-raid performance using Pokebattler Estimator, aka Average Scaled Estimator (ASE). Without and with dodging respectively.

Fire attackers ranked by their average in-raid performance using Pokebattler TTW, aka Average Scaled Time to Win (ASTTW). Without and with dodging respectively.

Fire attackers ranked by DPS^3*TDO and DPS respectively.




community day, litwick, chandelure, poltergeist, pve, raid attackers, ghost pve, dark pve, fire pve, ghost raid pokemon, dark raid pokemon, fire raid pokemon, ghost types, dark types, fire types, hydreigon, ghost meta, dark meta, fire meta,
